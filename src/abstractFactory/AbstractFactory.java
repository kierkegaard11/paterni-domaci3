/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractFactory;

import component.fields.AbstractPanelInput;
import view.FrmPerson;

/**
 *
 * @author maja
 */
public interface AbstractFactory {

    public static final String LBL_TEXT_FIRST_NAME = "First name: ";
    public static final String LBL_TEXT_LAST_NAME = "Last name: ";
    public static final String LBL_TEXT_GENDER = "Gender: ";
    public static final String LBL_TEXT_BIRTHDATE = "Birthdate: ";

    AbstractPanelInput createPanelInputFirstName();
    AbstractPanelInput createPanelInputLastName();
    AbstractPanelInput createPanelInputGender();
    AbstractPanelInput createPanelInputBirthdate();
}
