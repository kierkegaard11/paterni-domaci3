/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component.fields;

import java.awt.Component;
import java.awt.GridLayout;
import java.time.LocalDate;
import javax.print.attribute.standard.DateTimeAtCompleted;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author maja
 */
public class PanelInputMultipleComboBoxes extends AbstractPanelInput {

    private JPanel panel;
    private JComboBox<String> cmbDay;
    private JComboBox<String> cmbMonth;
    private JComboBox<String> cmbYear;

    public PanelInputMultipleComboBoxes(String lblName) {
        super(lblName);
    }

    @Override
    protected void abstractInitComponents() {
        panel = new JPanel();
        cmbDay = new JComboBox();
        cmbMonth = new JComboBox();
        cmbYear = new JComboBox();

        for (int i = 1; i < 32; i++) {
            cmbDay.addItem(i + "");
        }
        for (int i = 1; i < 13; i++) {
            cmbMonth.addItem(i + "");
        }
        for (int i = LocalDate.now().getYear(); i >= 1900; i--) {
            cmbYear.addItem(i + "");
        }

        panel.setLayout(new GridLayout(1, 3));
        panel.add(cmbDay);
        panel.add(cmbMonth);
        panel.add(cmbYear);
    }

    @Override
    protected Component getValueField() {
        return panel;
    }

    @Override
    public Object getValue() {
        int day = Integer.parseInt((String) cmbDay.getSelectedItem());
        int month = Integer.parseInt((String) cmbMonth.getSelectedItem());
        int year = Integer.parseInt((String) cmbYear.getSelectedItem());
        LocalDate date = LocalDate.of(year, month, day);
        return date;
    }

    @Override
    protected int getId() {
        return 2;
    }

    @Override
    public String toString() {
        return "Three combo boxes";
    }
}
