/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component.fields;

import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *
 * @author maja
 */
public class PanelInputRadioButton extends AbstractPanelInput {

    private static final String GENDER_M = "M";
    private static final String GENDER_F = "F";
    private JPanel panel;
    private ButtonGroup group;
    private JRadioButton buttonM;
    private JRadioButton buttonF;

    public PanelInputRadioButton(String lblNameText) {
        super(lblNameText);
    }

    @Override
    protected void abstractInitComponents() {
        panel = new JPanel();
        group = new ButtonGroup();
        buttonM = new JRadioButton();
        buttonF = new JRadioButton();
        buttonF.setSelected(true);
        buttonM.setText(GENDER_M);
        buttonF.setText(GENDER_F);

        panel.setLayout(new GridLayout(1, 2));

        group.add(buttonM);
        group.add(buttonF);
        panel.add(buttonM);
        panel.add(buttonF);

    }

    @Override
    protected Component getValueField() {
        return panel;
    }

    @Override
    public Object getValue() {
        return buttonM.isSelected() ? GENDER_M : GENDER_F;
    }

    @Override
    protected int getId() {
        return 4;
    }

    @Override
    public String toString() {
        return "Radio button";
    }
}
