/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component.fields;

import java.awt.Component;
import javax.swing.JComboBox;

/**
 *
 * @author maja
 */
public class PanelInputComboBox extends AbstractPanelInput {

    private JComboBox<String> cmb;

    public PanelInputComboBox(String lblNameText) {
        super(lblNameText);
    }

    @Override
    protected void abstractInitComponents() {
        cmb = new JComboBox<>();
        cmb.addItem("F");
        cmb.addItem("M");
    }

    @Override
    protected Component getValueField() {
        return cmb;
    }

    @Override
    public Object getValue() {
        return cmb.getSelectedItem();
    }

    @Override
    protected int getId() {
        return 1;
    }

    @Override
    public String toString() {
        return "Combo Box";
    }

    
}
