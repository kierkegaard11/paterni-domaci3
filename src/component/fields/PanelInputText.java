/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component.fields;

import java.awt.Component;
import javax.swing.JTextField;

/**
 *
 * @author maja
 */
public class PanelInputText extends AbstractPanelInput {

    private JTextField txtField;

    public PanelInputText(String lblNameText) {
        super(lblNameText);
    }

    @Override
    protected void abstractInitComponents() {
        txtField = new JTextField("");
    }

    @Override
    public Object getValue() {
        return txtField.getText().trim();
    }

    @Override
    protected Component getValueField() {
        return txtField;
    }

    @Override
    protected int getId() {
        return 5;
    }

    @Override
    public String toString() {
        return "Text field";
    }
}
