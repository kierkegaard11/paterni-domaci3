/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concreteFactory;

import abstractFactory.AbstractFactory;
import component.fields.AbstractPanelInput;
import component.fields.PanelInputText;

/**
 *
 * @author maja
 */
public class ConcreteFactory1 implements AbstractFactory {

    @Override
    public AbstractPanelInput createPanelInputFirstName() {
        return new PanelInputText(LBL_TEXT_FIRST_NAME);
    }

    @Override
    public AbstractPanelInput createPanelInputLastName() {
        return new PanelInputText(LBL_TEXT_LAST_NAME);
    }

    @Override
    public AbstractPanelInput createPanelInputGender() {
        return new PanelInputText(LBL_TEXT_GENDER);
    }

    @Override
    public AbstractPanelInput createPanelInputBirthdate() {
        return new PanelInputText(LBL_TEXT_BIRTHDATE);
    }

    @Override
    public String toString() {
        return "Configuration 1";
    }

}
